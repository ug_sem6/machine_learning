import matplotlib.pyplot as plt
import pandas as pd
import statistics
import csv

death_data = pd.read_csv("zgony.csv")
deaths = {}
for row in death_data.iterrows():
	if row[1]["Rok zgonu"] not in deaths:
		deaths[row[1]["Rok zgonu"]] = 0
	deaths[row[1]["Rok zgonu"]] += row[1]["Liczba zgonów"]

deaths_num_list = deaths.values()
print("Wartość oczekiwana (średnia):", sum(deaths_num_list) / len(deaths_num_list))
print("Mediana:", statistics.median(deaths_num_list))
print("Moda:", statistics.mode(deaths_num_list))
print("Wariancja:", statistics.variance(deaths_num_list))
print("Odchylenie standardowe:", statistics.stdev(deaths_num_list))

plt.bar(deaths.keys(), deaths.values())
plt.show()

