#! /usr/bin/env python3
import cv2
import imutils
import numpy as np
import matplotlib.pyplot as plt

# 1. wczytanie
img1 = cv2.imread("images/image1.jpg")
cv2.imshow("Original image", img1)
cv2.waitKey(0)

# 2. zapisanie
cv2.imwrite("image1_copy.jpg", img1)

# 3. wyciecie obrazu
rci = img1[150:300, 220:450]
cv2.imshow("RUI", rci)
cv2.imwrite("image1_cut.jpg", rci)
cv2.waitKey(0)

# 4. zmiana rozmiaru
img_resized = cv2.resize(img1, (400, 400))
cv2.imshow("Fixed Resizing", img_resized)
cv2.imwrite("image1_resized.jpg", img_resized)
cv2.waitKey(0)

# 5.obrót
img_rotated = imutils.rotate(img1, -45)
cv2.imshow("Imutils Rotation", img_rotated)
cv2.imwrite("image1_rotated.jpg", img_rotated)
cv2.waitKey(0)

# 6. rozmycie
img_blurred = cv2.blur(img1, (5, 5))
cv2.imshow("Blurred", img_blurred)
cv2.imwrite("image1_blurred.jpg", img_blurred)
cv2.waitKey(0)

# 7. złączenie dwóch obrazów
img_resized = imutils.resize(img1, width=460)
img_blurred_resized = imutils.resize(img_blurred, width=460)
img_joined = np.hstack((img_resized, img_blurred_resized))
cv2.imshow("Joined", img_joined)
cv2.imwrite("image1_joined.jpg", img_joined)
cv2.waitKey(0)

# 8. rysowanie
# prostokat
img_copy = img1.copy()
cv2.rectangle(img_copy, (300, 150), (520, 270), (0, 0, 255), 2)
cv2.imshow("rectangle", img_copy)
cv2.imwrite("image1_rectangle.jpg", img_copy)
cv2.waitKey(0)

# poligon, linia i koło
img = np.zeros((600, 600, 3), np.uint8)
points = np.array([[600, 200], [510, 641], [300, 300], [0, 0]])
cv2.polylines(img, np.int32([points]), 1, (255, 0, 255))
cv2.circle(img, (150, 200), 50, (0, 100, 255), 2)
cv2.line(img, (100, 0), (400, 400), (255, 0, 0), 5)

cv2.imshow("poly", img)
cv2.imwrite("image1_figures.jpg", img)
cv2.waitKey(0)

# umieszczenie tekstu na obrazie

font = cv2.FONT_HERSHEY_TRIPLEX

name = 'Tomasz Wolak'
cv2.putText(img1, name, (10, 100), font, 2, (255, 255, 255), 2, cv2.LINE_4)
cv2.imshow("Image with text", img1)
cv2.imwrite("image1_text.jpg", img1)
cv2.waitKey(0)
