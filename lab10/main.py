#! /usr/bin/env python3
import os
import signal
import sys

import cv2
import numpy
import imutils

face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
scaling_factor = 0.5
frame = cv2.imread("me.jpg")
# frame = cv2.resize(frame, None, fx=scaling_factor, fy=scaling_factor, interpolation=cv2.INTER_AREA)
face_rects = face_cascade.detectMultiScale(frame, scaleFactor=1.3, minNeighbors=5)
#
for (x, y, w, h) in face_rects:
    cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 3)
cv2.imshow("me.jpg", frame)
cv2.waitKey(0)
print(f"Found {len(face_rects)} faces!")

# wykrywanie twarzy uśmiechów i oczu ludzi
smile_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_smile.xml")
eye_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_eye.xml")

image = cv2.imread("people.jpg")
# image = cv2.resize(image, (1200, 800))
gray_filter = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
faces = face_cascade.detectMultiScale(image, scaleFactor=1.20, minNeighbors=3)

for (x, y, w, h) in faces:
    cv2.rectangle(image, (x, y), (x + w, y + h), (255, 0, 0), 2)
    roi_gray = gray_filter[y:y + h, x:x + w]
    roi_color = image[y:y + h, x:x + w]
    smile = smile_cascade.detectMultiScale(roi_color)
    eye = eye_cascade.detectMultiScale(roi_color)

    for (sx, sy, sw, sh) in smile:
        cv2.rectangle(roi_color, (sx, sy), (sx + sw, sy + sh), (0, 255, 0), 1)
    for (ex, ey, ew, eh) in eye:
        cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 0, 255), 1)
cv2.imshow("People", image)
# cv2.imshow("People gray", gray_filter)
cv2.waitKey(0)
print(f"Found {len(faces)} faces!")

# rozpoznanie twarzy z kamerki

cap = cv2.VideoCapture(0)

while True:
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # TODO: wykryć twarz na wideo z kamerki
    faces = face_cascade.detectMultiScale(frame, scaleFactor=1.20, minNeighbors=3)
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
    cv2.imshow('frame', frame)

    if cv2.waitKey(1) & 0XFF == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()
print("ok")
try:
    # rozpoznawanie osób na video
    # TODO: wykryć twarze
    hog = cv2.HOGDescriptor()
    hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
    cv2.startWindowThread()
    cap = cv2.VideoCapture("video4.mp4")

    while True:
        ret, frame = cap.read()
        frame = cv2.resize(frame, (1200, 800))
        boxes, weights = hog.detectMultiScale(frame, winStride=(8, 8))
        boxes = numpy.array([[x, y, x + w, y + h] for (x, y, w, h) in boxes])

        for (xa, ya, xb, yb) in boxes:
            cv2.rectangle(frame, (xa, ya), (xb, yb), (0, 255, 0), 2)
        faces = face_cascade.detectMultiScale(frame, scaleFactor=1.20, minNeighbors=3)

        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 1)

        cv2.imshow("Video", frame)
        print(f"Found {len(boxes)} people!")
        if cv2.waitKey(1) & 0XFF == ord('q'):
            break
    cap.release()
    cv2.destroyAllWindows()
except Exception as e:
    print(e)
finally:
    os.kill(os.getpid(), signal.SIGTERM)
