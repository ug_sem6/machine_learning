#! /usr/bin/env python3

text_data = ["    Wykrzyknik i znak zapytania, autor Aishwarya Henriette    ",
             "Techniki parkowania, autor Karl Gautier",
             "    Dzisiaj jest ta noc, autor Jarek Prakash    "]

strip_whitespace = [string.strip() for string in text_data]

print(strip_whitespace)

remove_periods = [string.replace("autor ", "") for string in strip_whitespace]

print(remove_periods)