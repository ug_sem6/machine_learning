#! /usr/bin/env python3
import collections
from copy import copy
from nltk.corpus import stopwords


stream = 'homepage/user, ' \
         'contact us/user, ' \
         'search products/user, add to cart/prospect, ' \
         'checkout/customer, ' \
         'register/customer, ' \
         'homepage/user, ' \
         'exit the page/user, ' \
         'homepage/user, ' \
         'searchproducts/user, ' \
         'add to cart/prospect, ' \
         'search products/prospect, ' \
         'search products/prospect, ' \
         'homepage/user, ' \
         'contact us/user, ' \
         'homepage/user, ' \
         'exit the page/user, ' \
         '\n '


class CorpusParser:
    NULL_CHARACTER = 'START'
    STOP = ', '
    SPLITTER = '/'

    def __init__(self):
        self.ngram = 2

    TagWord = collections.namedtuple('TagWord', ['word', 'tag'])

    def parse(self, stream):
        """
        Parse Brown Corpus
        :param stream: string of text
        :return: an iterator through ngraps
        """
        ngrams = self.ngram * [
            CorpusParser.TagWord(CorpusParser.NULL_CHARACTER, CorpusParser.NULL_CHARACTER)]
        word = ''
        pos = ''
        parse_word = True
        for char in stream:
            if char == '\t' or (len(word) == 0 and char in CorpusParser.STOP):
                continue
            elif char == CorpusParser.SPLITTER:
                parse_word = False
            elif char in CorpusParser.STOP:
                ngrams.pop(0)
                ngrams.append(CorpusParser.TagWord(word, pos))
                yield copy(ngrams)
                word = ''
                pos = ''
                parse_word = True
            elif parse_word:
                word += char
            else:
                pos += char
        if len(word) > 0 and len(pos) > 0:
            ngrams.pop(0)
            ngrams.append(CorpusParser.TagWord(word, pos))
            yield copy(ngrams)


stop_words = stopwords.words('english')

corpus = CorpusParser()
# print(list(corpus.parse(stream)))

parsed_stream = list(corpus.parse(stream))
train = parsed_stream[:10]
test = parsed_stream[10:]
from nltk.tag import UnigramTagger, BigramTagger, TrigramTagger

unigram = UnigramTagger(train)
bigram = BigramTagger(train, backoff=unigram)
trigram = TrigramTagger(train, backoff=bigram)
for t in train:
    print(trigram.tag(t))
print(trigram.evaluate(test))

from nltk.corpus import brown

sentences = brown.tagged_sents(categories='news')
# print(sentences[0])
