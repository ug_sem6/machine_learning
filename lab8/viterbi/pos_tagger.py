#! /usr/bin/env python3
from collections import defaultdict
import re
from corpus_parser import CorpusParser


class POSTagger:
    def __init__(self, data_io=(), eager=False):
        self.corpus_parser = CorpusParser()
        self.data_io = data_io
        self.trained = False
        self.tags = set()
        self.tag_combos = defaultdict(int)
        self.tag_frequencies = defaultdict(int)
        self.word_tag_combos = defaultdict(int)
        if eager:
            self.train()
            self.trained = True

    def train(self):
        if not self.trained:
            self.tags = set()
            self.tag_combos = defaultdict(int)
            self.tag_frequencies = defaultdict(int)
            self.word_tag_combos = defaultdict(int)
            for io in self.data_io:
                for line in io:
                    for ngram in self.corpus_parser.parse(line):
                        self.write(ngram)
            self.trained = True

    def write(self, ngram):
        """
        :param ngram:
        """
        if ngram[0].tag == 'START':
            self.tag_frequencies['START'] += 1
            self.word_tag_combos['START/START'] += 1
        self.tags.add(ngram[-1].tag)
        self.tag_frequencies[ngram[-1].tag] += 1
        combo = ngram[-1].word + '/' + ngram[-1].tag
        self.tag_combos[combo] += 1

    def tag_probability(self, previous_tag, current_tag):
        """Maximum likelihood estimate
        count(previous_tag, current_tag) / count(previous_tag)"""
        denom = self.tag_frequencies[previous_tag]
        if denom == 0:
            return 0
        else:
            return self.tag_combos[previous_tag + '/' + current_tag] / float(denom)

    def word_tag_probability(self, word, tag):
        denom = self.tag_frequencies[tag]
        if denom == 0:
            return 0
        else:
            return self.word_tag_combos[word + '/' + tag] / float(denom)

    def viterbi(self, sentence):
        sentence1 = re.sub(r'([\.\?!])', r' \1', sentence)
        parts = re.split(r'\s+', sentence1)
        last_viterbi = {}
        backpointers = ['START']
        for tag in self.tags:
            if tag == 'START':
                continue
            else:
                probability = self.tag_probability('START', tag) \
                              + self.word_tag_probability(parts[0], tag)
                if probability > 0:
                    last_viterbi[tag] = probability
            if len(last_viterbi) > 0:
                backpointer = max(self.tag_frequencies, key=(lambda key: self.tag_frequencies[key]))
                backpointers.append(backpointer)
        for part in parts[1:]:
            viterbi = {}
            for tag in self.tags:
                if tag == 'START':
                    continue
                if len(last_viterbi) == 0:
                    break
                best_tag = max(last_viterbi, key=(lambda prev_tag: last_viterbi[prev_tag] * self.tag_probability(prev_tag, tag) * self.word_tag_probability(part, tag)))
                probability = last_viterbi[best_tag] * \
                    self.tag_probability(best_tag, tag) * \
                    self.word_tag_probability(part, tag)
                if probability > 0:
                    viterbi[tag] = probability
                    last_viterbi = viterbi
                    if len(last_viterbi > 0):
                        backpointer = max(last_viterbi, key=(lambda key: last_viterbi[key]))
                    else:
                        backpointer = max(self.tag_frequencies, key=(lambda key: self.tag_frequencies[key]))
                    backpointers.append(backpointer)
                return backpointers


from io import StringIO
stream = u'homepage/user contact-us/user search-products/user add-to-cart/prospect checkout/customer register/customer homepage/user exit-the-page/user homepage/user search-products/user add-to-cart/prospect search-products/prospect search-products/prospect homepage/user contact-us/user homepage/user exit-the-page/user\n'
pos_tagger = POSTagger([StringIO(stream)])
pos_tagger.train()
print(pos_tagger.viterbi("homepage"))