#! /usr/bin/env python3
import numpy as np
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from nltk import pos_tag

class ForwardBackward:
    def __init__(self):
        self.observations = [
                '2021-03-12 12:04:32 user statek98 entered the home page. State: Prospect',
                '2021-03-12 12:06:47 user michal2 signed up. State: Prospect',
                '2021-03-12 12:08:12 user michal2 chose product. State: User',
                '2021-03-12 12:08:32 user statek98 chose product. State: User',
                '2021-03-12 12:18:25 user statek98 entered checkout page. State: User',
                '2021-03-12 12:20:10 user michal2 made purchase. State: Customer',
                '2021-03-13 14:15:35 user jkowalski43 entered the home page. State: User',
                '2021-03-15 10:15:35 user user2 entered the home page. State: User',
                '2021-03-14 10:26:47 user jkowalski43 chose product. State: Prospect',
                '2021-03-15 10:58:12 user jkowalski43 signed up. State: Prospect',
                '2021-03-15 11:28:05 user statek98 made contact. State: Customer',
                '2021-03-20 11:08:12 user user3 has made contact. State: Prospect'
        ]
        self.states = ['Prospect', 'User', 'Customer']
        self.emissions = ['homepage', 'signup', 'product page', 'checkout', 'contact us']
        self.start_probability = {
            'Prospect': 0.8,
            'User': 0.15,
            'Customer': 0.05
        }
        self.transition_probability = np.array([
            [0.8, 0.15, 0.05],
            [0.05, 0.80, 0.15],
            [0.02, 0.95, 0.03]
        ])
        self.emission_probability = np.array([
            [0.4, 0.3, 0.3],
            [0.1, 0.8, 0.1],
            [0.1, 0.3, 0.6],
            [0, 0.1, 0.9],
            [0.7, 0.1, 0.2]
        ])
        self.end_state = 'Ending'

    def forward(self):
        forward = []
        f_previous = {}
        for i in range(1, len(self.observations)):
            f_curr = {}
            for state in self.states:
                if i == 0:
                    prev_f_sum = self.start_probability[state]
                else:
                    prev_f_sum = 0.0
                    for k in self.states:
                        prev_f_sum += f_previous.get(k, 0.0) * self.transition_probability[k][state]
                f_curr[state] = self.emission_probability[state][self.observations[i]]
                f_curr[state] = f_curr[state] * prev_f_sum
                forward.append(f_curr)
        p_fwd = 0.0
        for k in self.states:
            p_fwd += f_previous[k] * self.transition_probability[k][self.end_state]
        return {'probability': p_fwd, 'sequence': forward}

    def backward(self):
        backward = []
        b_prev = {}
        for i in range(len(self.observations), 0, -1):
            b_curr = {}
            for state in self.states:
                if i == 0:
                    b_curr[state] = self.transition_probability[state][self.end_state]
                else:
                    sum = 0.0
                    for k in self.states:
                        sum += self.transition_probability[state][k] * self.emission_probability[k][
                            self.observations[-i]] * b_prev[k]
                backward.insert(0, b_curr)
            p_bkw = 0.0
            for s in self.states:
                sum += self.start_probability[s] * self.emission_probability[s][
                    self.observations[0]] * b_prev[s]
        return {'probability': p_bkw, 'sequence': backward}

    def forward_backward(self):
        size = len(self.observations)
        forward = self.forward()
        backward = self.backward()
        posterior = {}
        for s in self.states:
            posterior[s] = []
            for i in range(1, size):
                value = forward['sequence'][i][s] * backward['sequence'][i][s] / forward[
                    'probability']
            posterior.append()
            return [forward, backward, posterior]
    def process(self):
        processed_obervations = []
        for observation in self.observations:
            new_observation = []
            tokenized_observation = sent_tokenize(observation)
            for sent in tokenized_observation:
                tokenized_words = word_tokenize(sent)
                stop_words = stopwords.words('english')
                porter = PorterStemmer()
                stemmed_words = [porter.stem(word) for word in tokenized_words if word not in stop_words]
                tagged = pos_tag(tokenized_words)

