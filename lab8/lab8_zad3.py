#! /usr/bin/env python3
import nltk
# nltk.download(['stopwords','averaged_perceptron_tagger'])

# tokenizacja tekstu (podzielenie tekstu na pojedyncze słowa/zdania)
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from nltk import pos_tag

from nltk.tag import BigramTagger, TrigramTagger, UnigramTagger

observations = [
    ('homepage', 'user'),
    ('contact us', 'user'),
    ('search products', 'user'),
    ('add to cart', 'prospect'),
    ('checkout', 'customer'),
    ('register', 'customer'),
    ('homepage', 'user'),
    ('exit the page', 'user'),
    ('homepage', 'user'),
    ('search products', 'user'),
    ('add to cart', 'prospect'),
    ('search products', 'prospect'),
    ('search products', 'prospect'),
    ('homepage', 'user'),
    ('contact us', 'user'),
    ('homepage', 'user'),
    ('exit the page', 'user'),

]

stop_words = stopwords.words('english')
train = observations
test = observations

test_tagged = [pos_tag(word) for word in test]
train_tagged = [pos_tag(word) for word in train]


unigram = UnigramTagger(train_tagged)
bigram = BigramTagger(train_tagged, backoff=unigram)
trigram = TrigramTagger(train_tagged, backoff=bigram)
print(trigram.tag(test_tagged[0]))
print(trigram.evaluate(test[:2]))
