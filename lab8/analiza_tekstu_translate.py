#! /usr/bin/env python3
import unicodedata
import sys

text_data = ['Cześć!!! Po. Prostu. Kocham. Tę. Piosenkę....',
             'Zgadzam się w 100%!!!! #KochamTO',
             'Racja?!?!']

# utworzenie słownika znaków przystankowychow
punctuation = dict.fromkeys(i for i in range(sys.maxunicode)
                            if unicodedata.category(chr(i)).startswith('P'))

print([string.translate(punctuation) for string in text_data])

import nltk
nltk.download(['stopwords','averaged_perceptron_tagger'])

# tokenizacja tekstu (podzielenie tekstu na pojedyncze słowa/zdania)
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from nltk import pos_tag

string = "User of name user1 has entered the homepage."
tokenized_words = word_tokenize(string)
print(tokenized_words)

string = "User of name 'user1' has entered the homepage."
tokenized_sents = sent_tokenize(string)
print(tokenized_sents)

# usunięcie słów o małym znaczeniu
stop_words = stopwords.words('english')

print([word for word in tokenized_words if word not in stop_words])

# stemming słów (konwertowanie słów na podstawowe formy

porter = PorterStemmer()
print([porter.stem(word) for word in tokenized_words])

# oznaczanie części mowy
text_tagged = pos_tag(tokenized_words)
print(text_tagged)
print([word for word, tag in text_tagged if tag in ['NN', 'NNS', 'NNP', 'NNPS']])
