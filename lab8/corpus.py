#! /usr/bin/env python3
from nltk.corpus import brown
from nltk.tag import UnigramTagger, BigramTagger, TrigramTagger

sentences = brown.tagged_sents(categories='news')

train = sentences[:4000]
print(train, end="\n\n")
test = sentences[4000:]
print(test)
unigram = UnigramTagger(train)
bigram = BigramTagger(train, backoff=unigram)
trigram = TrigramTagger(train, backoff=bigram)

print(trigram.evaluate(test))
#
# import unittest
# from hidden_markov_model.corpus.parser import CorpusParser
#
# class TestCorpusParser(unittest.TestCase):
#     def setUp(self):
#         self.stream = '\tSeveral/ap defendants/nns ./.\n'
#         self.blank = '\t    \n'
#     def test_parse(self):
#         """will parse a bronw corpus line using the standard / notation"""
#         cp = CorpusParser()
#         null = CorpusParser.TagWord('START', 'START')
#         several = CorpusParser.TagWord('Several', 'ap')
#         defendants = CorpusParser.TagWord('defendants', 'nns')
#         period = CorpusParser.TagWord('.', '.')
#         expectations = [
#             [null, several],
#             [several, defendants],
#             [defendants, period]
#         ]
#         resuls = list(cp.parse(self.stream))
#         self.assertListEqual((expectations, resuls))
