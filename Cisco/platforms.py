from platform import \
	platform, machine, processor, system, version, \
	python_implementation, python_version_tuple

print(platform())
print(platform(aliased=True))
print(platform(aliased=True, terse=True))

print("Machine type:", machine(), end="\n\n")
print("Processor:", processor(), end="\n\n")

print("System:", system())
print("OS version: ", version(), end="\n\n")

print("Python implementation:", python_implementation())

for atr in python_version_tuple():
	print(python_version_tuple(), sep=".")