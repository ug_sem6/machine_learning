import pandas as pd
import random
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import KFold


class Adaline:
	def __init__(self, data, weights=None, learning_rate=0.0001):
		if weights is None:
			weights = [.0, .0, .0]
		self.costs = []
		self.data = data
		self.weights = weights
		self.learning_rate = learning_rate
		self.n = len(data)  # ile jest próbek
		self.predictions = []  #
		self.expected = [row[-1] for row in self.data]

	def activation_function(self, value):
		return value

	def cost_function(self, error):
		return error ** 2

	def predict(self, row, bias):
		summation = bias
		x = [row[i] for i in range(len(row) - 1)]
		for i in range(len(x)):
			summation += self.weights[i + 1] * x[i]
		return self.activation_function(summation)

	def train(self, epoch):
		global accuracy_list
		accuracy_list = []
		self.costs = []
		cost = .0
		for t in range(epoch):
			errors = []
			predictions = []
			np.random.shuffle(self.data)
			for row in self.data:
				prediction = self.predict(row, self.weights[0])  # wartość końcowa dla i-tej próbki (y^(i))
				expected = row[-1]
				error = expected - prediction
				errors.append(error)
				predictions.append(prediction)
				# różnica między gradientem prostym - aktualizacja wag dla każdej próbki
				cost = self.cost_function(error)
				self.weights[0] += self.learning_rate * error
				for i in range(1, len(self.weights)):
					self.weights[i] += self.learning_rate * error * row[i - 1]
			# print(self.weights)
			print("Loss on epoch", t+1, ":", cost)
			self.costs.append(cost)  # obliczanie kosztu funkcji


dataframe = pd.read_csv("iris.data", header=None)

print(dataframe.tail())

classification = {
	"Iris-setosa": 0.0,
	"Iris-versicolor": 1.0
}

# filtrujemy (nie powinno byc  klasy Iris-virginica)
iris_data = np.array(list(filter(lambda data: data[1][4] != "Iris-virginica", dataframe.iterrows())))
np.random.shuffle(iris_data)

# wybieramy potrzebne dane (dlugość działki i długość płatka i klasa pozytywna/negatywna)
iris_data = np.array([[row[1][0], row[1][2], classification[row[1][4]]] for row in iris_data])
a = Adaline(iris_data)

print("Learning rate:", a.learning_rate)

epochs = 75
a.train(epochs)

print(f"\nFinal loss: {a.costs[-1]}")
min_loss = min(a.costs)
print(f"Minimum loss: {min_loss} (epoch {a.costs.index(min_loss) + 1})")

plt.plot(range(epochs), a.costs, marker='o')
plt.xlabel("Epochs")
plt.ylabel("Loss")
plt.show()
