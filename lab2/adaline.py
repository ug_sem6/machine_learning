import pandas as pd
import random
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import KFold


class Adaline:
	def __init__(self, data, weights=[.0, .0, .0], learning_rate=0.0001):
		self.costs = []
		self.data = data
		self.weights = weights
		self.learning_rate = learning_rate
		self.n = len(data)  # ile jest próbek
		self.predictions = []  #
		self.expected = [row[-1] for row in self.data]

	def activation_function(self, value):
		return value

	def cost_function(self, errors):
		return 0.5 * sum([e ** 2 for e in errors])

	def predict(self, row, bias):
		summation = bias
		x = [row[i] for i in range(len(row) - 1)]
		for i in range(len(x)):
			summation += self.weights[i + 1] * x[i]
		return self.activation_function(summation)

	def train(self, epoch):
		global accuracy_list
		accuracy_list = []
		self.costs = []
		for t in range(epoch):
			errors = []
			predictions = []
			np.random.shuffle(self.data)

			for row in self.data:
				prediction = self.predict(row, self.weights[0]) # wartość końcowa dla i-tej próbki (y^(i))
				expected = row[-1]
				error = expected - prediction
				errors.append(error)
				predictions.append(prediction)
			self.costs.append(self.cost_function(errors)) # obliczanie kosztu funkcji
			self.weights[0] += self.learning_rate * sum([errors[j] * self.data[j][0] for j in range(len(errors))])
			for i in range(1, len(self.weights)):
				self.weights[i] += self.learning_rate * sum([errors[j] * self.data[j][i - 1] for j in range(len(errors))])

dataframe = pd.read_csv("iris.data", header=None)

print(dataframe.tail())

classification = {
	"Iris-setosa": 0.0,
	"Iris-versicolor": 1.0
}

# filtrujemy (nie powinno byc  klasy Iris-virginica)
iris_data = np.array(list(filter(lambda data: data[1][4] != "Iris-virginica", dataframe.iterrows())))
np.random.shuffle(iris_data)

# wybieramy potrzebne dane (dlugość działki i długość płatka i klasa pozytywna/negatywna)
iris_data = np.array([[row[1][0], row[1][2], classification[row[1][4]]] for row in iris_data])
a = Adaline(iris_data)

print("Learning rate:", a.learning_rate)
print("Start weights:", a.weights)

epochs = 50
a.train(epochs)

print("\nFinal cost:", a.costs[-1])
min_cost = min(a.costs)
print(f"Minimum cost: {min_cost} (epoch {a.costs.index(min_cost) + 1})")

plt.plot(range(epochs), a.costs, marker='o')
plt.xlabel("Epoch")
plt.ylabel("Cost")
plt.show()