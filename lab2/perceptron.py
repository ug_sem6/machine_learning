import pandas as pd
import random
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import KFold


class Perceptron:
	def __init__(self, data, weights=[.0, .0, .0], learning_rate=0.01):
		self.data = data
		self.weights = weights
		self.learning_rate = learning_rate
		self.n = len(iris_data)  # ile jest próbek
		self.predictions = []  #
		self.expected = [row[-1] for row in self.data]

	def activation_function(self, value):
		return 1.0 if value > self.weights[0] else 0.0

	def predict(self, row):
		summation = self.weights[0]
		x = [row[i] for i in range(len(row) - 1)]
		for i in range(len(x)):
			summation += self.weights[i + 1] * x[i]
		return self.activation_function(summation)

	def accuracy_metric(self):
		correct = 0
		for i in range(len(self.expected)):
			if self.expected[i] == self.predictions[i]:
				correct += 1
		return correct / float(len(self.expected)) * 100.0

	def train(self, epoch):
		global accuracy_list
		accuracy_list = []
		for t in range(epoch):
			for row in self.data:
				prediction = self.predict(row)
				expected = row[-1]
				error = expected - prediction
				self.predictions.append(prediction)
				self.weights[0] += self.learning_rate * error
				for i in range(1, len(self.weights)):
					self.weights[i] += self.learning_rate * error * row[i - 1]
			accuracy_list.append(self.accuracy_metric())


dataframe = pd.read_csv("iris.data", header=None)

print(dataframe.tail())

classification = {
	"Iris-setosa": 0.0,
	"Iris-versicolor": 1.0
}

# filtrujemy (nie powinno byc  klasy Iris-virginica)
iris_data = np.array(list(filter(lambda data: data[1][4] != "Iris-virginica", dataframe.iterrows())))
np.random.shuffle(iris_data)

# wybieramy potrzebne dane (dlugość działki i długość płatka i klasa pozytywna/negatywna)
iris_data = np.array([[row[1][0], row[1][2], classification[row[1][4]]] for row in iris_data])
p = Perceptron(iris_data)

print("Learning rate:", p.learning_rate)
print("Start weights:", p.weights)

epochs = 10
p.train(epochs)

print("\nFinal weights:", p.weights)
print("Accuracy:", p.accuracy_metric())
print(sum(accuracy_list) / len(accuracy_list))

x = [row[0] for row in iris_data]  # sepal length
y = [row[1] for row in iris_data]  # petal length

plt.scatter(x, y, c=["yellow" if row[2] == 1.0 else "green" for row in iris_data])

plt.xticks(np.arange(min(x), max(x), 0.3))
plt.yticks(np.arange(min(y), max(y), 0.5))

plt.xlabel("Sepal length")
plt.ylabel("Petal length")

plt.show()

# dokładność modelu (zależy od losowego rozstawienia danych)
print(len(range(epochs)))
print(len(accuracy_list))
plt.scatter(range(epochs), accuracy_list)
plt.xlabel("Epochs")
plt.ylabel("Perceptron accuracy")
plt.show()
