#! /usr/bin/env python3

import cv2
import numpy as np
from matplotlib import pyplot as plt

# pobranie i zapisanie ramki z wideo
cap = cv2.VideoCapture("video1.mp4")
ret, img = cap.read()
cv2.resize(img, (600, 400))
cv2.imwrite("img1.jpg", img)

# 1. pobranie obrazu
img = cv2.imread("img1.jpg")
cv2.imshow("Input image", img)
cv2.waitKey(0)

# 2. pobranie z konwersją na czarno-biały
gray_img = cv2.imread("img1.jpg", cv2.IMREAD_GRAYSCALE)
cv2.imwrite("img-gray.jpg", gray_img)

cv2.imshow("Gray input image", gray_img)
cv2.waitKey(0)
cv2.destroyAllWindows()

# 3. rozmycie przez filtr Gaussa
blur_kernel_size = (15, 15)
img = cv2.imread("img1.jpg")

blur = cv2.GaussianBlur(img, blur_kernel_size, 0)
cv2.imshow("Image blur", blur)
cv2.imwrite("img-blur.jpg", blur)
cv2.waitKey(0)

# 4. dobór konturów alg Kenny'ego
canny_low_threshold = 10
canny_high_threshold = 50

img = cv2.imread("img1.jpg")

img_canny = cv2.Canny(img, canny_low_threshold, canny_high_threshold)
cv2.imshow("Image Canny", img_canny)
cv2.imwrite("img-canny.jpg", img_canny)
cv2.waitKey(0)

# 5.wybranie konturów na obrazie
img = cv2.imread("img-canny.jpg", 0)
height, width = img.shape[:2]
print(img.shape[:2])

h = 220
w = 1280
x = 500
y = 0
img1 = img[x:x + h, y:y + w]
img2 = np.zeros_like(img)
img2[x:x + h, y:y + w] = img1
cv2.imshow("contours", img2)
cv2.imwrite("img-contour.jpg", img2)
cv2.waitKey(0)
cv2.destroyAllWindows()

# wybór linii prostych algorytmem Huffa
import math
import cv2 as cv
import numpy as np

src = cv2.imread('img1.jpg')
dst = cv.Canny(src, 50, 200, None, 3)
cdst = cv.cvtColor(dst, cv.COLOR_GRAY2BGR)
cdstP = np.copy(cdst)
lines = cv.HoughLines(dst, 1, np.pi / 180, 150, None, 0, 0)
if lines is not None:
    for i in range(len(lines)):
        rho = lines[i][0][0]
        theta = lines[i][0][1]
        a = math.cos(theta)
        b = math.sin(theta)
        x0 = a * rho
        y0 = b * rho
        pt1 = (int(x0 + 1000 * (-b)), int(y0 + 1000 * a))
        pt2 = (int(x0 - 1000 * (-b)), int(y0 - 1000 * a))
        cv.line(cdst, pt1, pt2, (0, 0, 255), 3, cv.LINE_AA)
linesP = cv.HoughLinesP(dst, 1, np.pi / 180, 50, None, 50, 10)
if linesP is not None:
    for i in range(len(linesP)):
        l = linesP[i][0]
        cv.line(cdstP, (l[0], l[1]), (l[2], l[3]), (0, 0, 255), 3, cv.LINE_AA)
cv.imshow("Detected lines (in red)", cdstP)
cv2.imwrite("red-lines.jpg", cdstP)
cv2.waitKey(0)
cv2.destroyAllWindows()

img = cv2.imread("img1.jpg")
img1 = cv2.imread("red-lines.jpg")
img2 = cv2.addWeighted(img, 0.8, img1, 1, 0)
cv2.imshow("sum", img2)
cv2.imwrite("img-org-red.jpg", img2)
cv2.waitKey(0)
cv2.destroyAllWindows()


while True:
    ret, src = cap.read()
    dst = cv.Canny(src, 50, 200, None, 3)
    cdst = cv.cvtColor(dst, cv.COLOR_GRAY2BGR)
    cdstP = np.copy(cdst)
    lines = cv.HoughLines(dst, 1, np.pi / 180, 150, None, 0, 0)
    if lines is not None:
        for i in range(len(lines)):
            rho = lines[i][0][0]
            theta = lines[i][0][1]
            a = math.cos(theta)
            b = math.sin(theta)
            x0 = a * rho
            y0 = b * rho
            pt1 = (int(x0 + 1000 * (-b)), int(y0 + 1000 * a))
            pt2 = (int(x0 - 1000 * (-b)), int(y0 - 1000 * a))
            cv.line(cdst, pt1, pt2, (0, 0, 255), 3, cv.LINE_AA)
    linesP = cv.HoughLinesP(dst, 1, np.pi / 180, 50, None, 50, 10)
    if linesP is not None:
        for i in range(len(linesP)):
            l = linesP[i][0]
            cv.line(cdstP, (l[0], l[1]), (l[2], l[3]), (0, 0, 255), 3, cv.LINE_AA)
    img2 = cv2.addWeighted(src, 0.8, cdstP, 1, 0)
    name = 'Tomasz Wolak'
    font = cv2.FONT_HERSHEY_TRIPLEX
    cv2.putText(img2, name, (800, 700), font, 2, (255, 255, 255), 2, cv2.LINE_4)
    cv2.imshow("Video", img2)
    if cv2.waitKey(1) & 0XFF == ord('q'):
        break

cv2.destroyAllWindows()
